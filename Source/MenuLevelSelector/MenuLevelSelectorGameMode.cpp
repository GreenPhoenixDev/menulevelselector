// Copyright Epic Games, Inc. All Rights Reserved.

#include "MenuLevelSelectorGameMode.h"
#include "MenuLevelSelectorPlayerController.h"
#include "MenuLevelSelectorCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"

AMenuLevelSelectorGameMode::AMenuLevelSelectorGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AMenuLevelSelectorPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownTemplate/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

void AMenuLevelSelectorGameMode::BeginPlay()
{
	// setup and display the instructions widget
	if (!ensure(InputInstructionsWidgetClass != nullptr)) return;

	InputInstructionsWidget = CreateWidget<UUserWidget>(GetWorld(), InputInstructionsWidgetClass);
	if (!ensure(InputInstructionsWidget != nullptr)) return;

	InputInstructionsWidget->bIsFocusable = true;
	InputInstructionsWidget->AddToViewport();

	FInputModeGameAndUI InputModeData;
	InputModeData.SetWidgetToFocus(InputInstructionsWidget->TakeWidget());

	APlayerController* PC = GetWorld()->GetFirstPlayerController();
	if (!ensure(PC != nullptr)) return;

	// We always want to have both the Input from the Game and the UI
	PC->SetInputMode(InputModeData);
}