// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/PromptWidget.h"
#include "UI/MenuWidget.h"

#include "Components/Button.h"
#include "Components/TextBlock.h"

void UPromptWidget::Setup(UMenuWidget* InParent, FString& PromptMessageString, FString& ConfirmString, FString& DeclineString)
{
	Parent = InParent;

	if (!ensure(Button_Confirm != nullptr)) return;
	Button_Confirm->OnClicked.AddDynamic(this, &UPromptWidget::Confirm);

	if (!ensure(Parent != nullptr)) return;
	Parent->bIsFocusable = false;
	Parent->SetIsEnabled(false);
	this->bIsFocusable = true;
	this->AddToViewport();

	FInputModeGameAndUI InputModeData;
	InputModeData.SetWidgetToFocus(this->TakeWidget());

	PromptMessage = PromptMessageString;
	ButtonConfirmString = ConfirmString;
	ButtonDeclineString = DeclineString;

	TextBlock_PromptMessage->SetText(FText::FromString(PromptMessage));
	TextBlock_ConfirmString->SetText(FText::FromString(ButtonConfirmString));
}

void UPromptWidget::Confirm()
{
	if (!ensure(Parent != nullptr)) return;

	Parent->D_OnPromptSelected.Broadcast(PromptState::Confirmed);
	Parent->SetIsEnabled(true);

	RemovePrompt();
}

void UPromptWidget::Decline()
{
	if (!ensure(Parent != nullptr)) return;

	Parent->D_OnPromptSelected.Broadcast(PromptState::Declined);
	Parent->SetIsEnabled(true);

	RemovePrompt();
}

void UPromptWidget::RemovePrompt()
{
	if (!ensure(Parent != nullptr)) return;
	Parent->bIsFocusable = true;
	this->bIsFocusable = false;
	this->RemoveFromViewport();

}